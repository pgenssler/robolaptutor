Template für ein Project in PyCharm.

Anleitung:
1. Installiere PyCharm https://www.jetbrains.com/pycharm/download/
2. Installiere das Pycharm Plugin BashSupport 
  File -> Settings -> Plugins
  Klicke auf "Browse repositories" und suche nach "BashSupport".
3. Erstelle ein neues Project. 
4. Kopiere den src Ordner und das ev3devCopy.sh Skript in den Projektordner.
5. In PyCharm: Klicke rechts auf das Skript und wähle "run ev3devCopy.sh"
6. Es kommt eine Ausgabe ähnlich dieser
       UP : released
     DOWN : released
     LEFT : released
    RIGHT : released
    ENTER : released
BACKSPACE : released

Zu beachten
1. Alle Python datei werden in src Ordner abgelegt. Nur dieser wird kopiert.
2. Es werden nur ".py" Datein kopiert.
3. Es muss eine main funktion enthalten sein. (vergleiche main.py)

FAQ:
1. Wie ändern wir das Passwort?
In der Skript datei gibt es eine Variable password, in Zeile 2, dort kann das Passwort gesetzt werden.

