#!/bin/bash
password=maker
echo "Delete old files"
sshpass -p $password ssh robot@ev3dev.local 'rm ./*py;'
echo "Copy Folder ${PWD#/} to Robot"
sshpass -p $password scp ./src/*.py robot@ev3dev.local:~/
echo "Copied finished"
echo "Start Program:"
sshpass -p $password ssh robot@ev3dev.local 'files=$(find -name "*.py" -maxdepth 1); python $files'
