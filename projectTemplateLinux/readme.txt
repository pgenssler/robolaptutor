Linux Template für ein Projekt in PyCharm

Anleitung
0. Befolge die Schritte gewissenhaft, wenn etwas nicht geht oder Du es nicht verstehst/Dir unsicher bist, dann frage besser gleich Deinen Tutor
1. Installiere PyCharm Community Edition https://www.jetbrains.com/pycharm/download/
2. Starte PyCharm und wähle "Check out from Version Control" -> Git
     URL: https://bitbucket.org/SebastianHahn/robolaptutor
3. Installiere das PyCharm Plugin BashSupport
    File -> Settings -> Plugins
    Klicke auf "Browse repositories" und suche nach "BashSupport"
4. Bestätige, dass PyCharm neu gestartet werden soll
5. Ändere in der ev3devCopy.sh das Passwort und setze die IP des Roboters ein
5. In PyCharm: Öffne links den Ordner "projectTemplateLinux" und klicke rechts auf das Skript "ev3devCopy.sh" und wähle "run ev3devCopy.sh"
6. Es kommt eine Ausgabe ähnlich dieser
       UP : released
     DOWN : released
     LEFT : released
    RIGHT : released
    ENTER : released
BACKSPACE : released
7. Von hier aus kannst Du Dein eigenen Code schreiben, in main.py mit der Methode "def main():" geht es los
(8. Lösche die anderen Ordner "projectTemplateWindows" und "projectTemplate")

Zu beachten
1. Alle Python Datei müssen im "src" Ordner abgelegt werden. Nur dieser wird kopiert.
2. Es werden nur ".py" Datein kopiert.
3. Es muss eine main funktion enthalten sein. (vergleiche main.py)
4. Bis zum eigentlichen Ausführen des Scriptes dauert es ein paar Sekunden

FAQ:
1. Wie ändern wir das Passwort?
In der Skript Datei gibt es eine Variable password, in Zeile 2, dort kann das Passwort gesetzt werden.
