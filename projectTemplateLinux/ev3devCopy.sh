#!/bin/bash

# change password and roboip
password=maker
roboip=172.16.XXX.XXX

if [ $roboip = "172.16.XXX.XXX" ]; then
    echo "set IP address in this script first"
else
    echo "Delete old files"
    sshpass -p $password ssh robot@$roboip 'rm ./*py;'
    echo "Copy Folder ${PWD#/} to Robot"
    sshpass -p $password scp ./src/*.py robot@$roboip:~/
    echo "Copied finished"
    echo "Start Program:"
    sshpass -p $password ssh robot@$roboip 'files=$(find -name "*.py" -maxdepth 1); python $files'
fi
