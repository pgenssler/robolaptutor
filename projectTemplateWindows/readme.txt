Windows Template für ein Projekt in PyCharm

Anleitung
0. Befolge die Schritte gewissenhaft, wenn etwas nicht geht oder Du es nicht verstehst/Dir unsicher bist, dann frage besser gleich Deinen Tutor
1. Installiere Python (https://www.python.org/downloads/)
2. Installiere PyCharm Community Edition (https://www.jetbrains.com/pycharm/download/)
3. Starte PyCharm und erstelle ein neues Projekt
4. Kopiere den Inhalt aus dem Ordner projectTemplateWindows in Dein neues Projekt
5. Installiere das PyCharm Plugin cmdsupport
     File -> Settings -> Plugins
     Klicke auf "Browse repositories" und suche nach "cmd support"
5. Bestätige, dass PyCharm neu gestartet werden soll
6. Ändere in der ev3Copy.bat das Passwort und setze die IP des Roboters ein
7. In PyCharm: Klicke rechts auf das Skript "ev3Copy.bat" und wähle "run cmd script"
8. Es kommt eine Ausgabe ähnlich dieser
       UP : released
     DOWN : released
     LEFT : released
    RIGHT : released
    ENTER : released
BACKSPACE : released
9. Von hier aus kannst Du Dein eigenen Code schreiben, in main.py mit der Methode "def main():" geht es los

Zu beachten
1. Alle Python Datei müssen im "src" Ordner abgelegt werden. Nur dieser wird kopiert.
2. Es werden nur ".py" Datein kopiert.
3. Es muss eine main funktion enthalten sein. (vergleiche main.py)
4. Bis zum eigentlichen Ausführen des Scriptes dauert es ein paar Sekunden

FAQ:
1. Wie ändern wir das Passwort?
In der Skript Datei gibt es eine Variable password, in Zeile 2, dort kann das Passwort gesetzt werden.
